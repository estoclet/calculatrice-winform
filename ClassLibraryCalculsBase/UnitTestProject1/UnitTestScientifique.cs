﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClassLibraryCalculs;

namespace UnitTestStandards
{
    [TestClass]
    public class UnitTestScientifique
    {
        [TestMethod]
        public void TestInverseEntier()
        {
            float resultat = Scientifique.Inverse(5);
            Assert.AreEqual(resultat, 0.2f);
        }

        [TestMethod]
        public void TestInverseReel()
        {
            float resultat = Scientifique.Inverse(2.5f);
            Assert.AreEqual(resultat, 0.4f);
        }

        [TestMethod]
        [ExpectedException(typeof(DivideByZeroException))]
        public void TestInverseException()
        {
            float resultat = Scientifique.Inverse(0);
        }

        //test de la metode racine carrée
        [TestMethod]
        public void TestRacineCarreeEntier()
        {
            double resultat = Scientifique.RacineCarree(81);
            Assert.AreEqual(resultat, 9);
        }
        [TestMethod]
        public void TestRacineCarreeReel()
        {
            double resultat = Scientifique.RacineCarree(5.175625);
            Assert.AreEqual(resultat, 2.275);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestRacineCarreeExceptionValeurNegative()
        {
            double resultat = Scientifique.RacineCarree(-1.33);
            
        }

        // test de la méthode carré
        [TestMethod]
        public void TestCarreEntierPositif()
        {
            float resultat = Scientifique.Carre(9);
            Assert.AreEqual(resultat, 81);
        }
        [TestMethod]
        public void TestCarreEntierNegatif()
        {
            float resultat = Scientifique.Carre(-9);
            Assert.AreEqual(resultat, 81);
        }
        [TestMethod]
        public void TestCarreeReelPositif()
        {
            float resultat = Scientifique.Carre(1.63f);
            Assert.AreEqual(resultat, 2.6569f);
        }
        [TestMethod]
        public void TestCarreeReelNegatif()
        {
            float resultat = Scientifique.Carre(-1.63f);
            Assert.AreEqual(resultat, 2.6569f);
        }
        //test de la méthode puissance
        [TestMethod]
        public void TestPuissanceEntierPositifExposantPositif()
        {
            double resultat = Scientifique.PuissanceN(5,5);
            Assert.AreEqual(resultat, 3125);
        }

        [TestMethod]
        public void TestPuissanceEntierNegatifExposantPositif()
        {
            double resultat = Scientifique.PuissanceN(-5, 5);
            Assert.AreEqual(resultat, - 3125);
        }
        [TestMethod]
        public void TestPuissanceEntierPositifExposantNegatif()
        {
            double resultat = Scientifique.PuissanceN(5, - 5);
            Assert.AreEqual(resultat, 0.00032);
        }
        [TestMethod]
        public void TestPuissanceEntierNegatifExposantNegatif()
        {
            double resultat = Scientifique.PuissanceN( -5, -5);
            Assert.AreEqual(resultat, -0.00032);
        }

        [TestMethod]
        public void TestPuissanceReelPositifExposantPositif()
        {
            double resultat = Scientifique.PuissanceN(5.5, 5);
            Assert.AreEqual(resultat, 5032.84375);

        }

        [TestMethod]
        public void TestPuissanceReelNegatifExposantPositif()
        {
            double resultat = Scientifique.PuissanceN(-5.5, 5);
            Assert.AreEqual(resultat, -5032.84375);

        }

        [TestMethod]
        public void TestPuissanceReelPositifExposantNegatif()
        {
            double resultat = Scientifique.PuissanceN(2.25, - 3);
            Assert.AreEqual(resultat, 0.087791495,0.0000001);
        }

        [TestMethod]
        public void TestPuissanceReelNegatifExposantNegatif()
        {
            double resultat = Scientifique.PuissanceN(-2, -3);
            Assert.AreEqual(resultat, -0.125);
        }

        // test methode LogBase10
        [TestMethod]
        public void TestLogBase10EntierPositif()
        {
            double resultat = Scientifique.LogBase10(10);
            Assert.AreEqual(resultat, 1);
        }

        [TestMethod]
        public void TestLogBase10ReelrPositif()
        {
            double resultat = Scientifique.LogBase10(2154.2693374042365991198893365562);
            Assert.AreEqual(resultat,3.3333);
        }

        [TestMethod]
        [ExpectedException (typeof(ArithmeticException))]
        public void TestLogBase10DeZero()
        {
            double resultat = Scientifique.LogBase10(0);
            
        }
        [TestMethod]
        [ExpectedException(typeof(ArithmeticException))]
        public void TestLogBase10Negatif()
        {
            double resultat = Scientifique.LogBase10(-5);

        }

        // test méthode LogBaseE
        [TestMethod]
        public void TestLogBaseEEntierPositif()
        {
            double resultat = Scientifique.LogBaseE(3);
            Assert.AreEqual(resultat, 1.0986122886681096913952452369225);
        }
        [TestMethod]
        public void TestLogBaseEReelrPositif()
        {
            double resultat = Scientifique.LogBaseE(2.7182818284590452353602874713527);
            Assert.AreEqual(resultat, 1);
        }


        [TestMethod]
        [ExpectedException(typeof(ArithmeticException))]
        public void TestLogBaseEDeZero()
        {
            double resultat = Scientifique.LogBaseE(0);

        }
        [TestMethod]
        [ExpectedException(typeof(ArithmeticException))]
        public void TestLogBaseENegatif()
        {
            double resultat = Scientifique.LogBaseE(-5);

        }
        //test de la méthode exponentielle

        [TestMethod]
        public void TestExponentielleEntierPositif()
        {
            double resultat = Scientifique.ExponentielleBAseE(1);
            Assert.AreEqual(resultat, 2.7182818284590452353602874713527);
        }
        [TestMethod]
        public void TestExponentielleEntierNegatif()
        {
            double resultat = Scientifique.ExponentielleBAseE(-1);
            Assert.AreEqual(resultat, 0.367879441,0.000000001);
        }
        [TestMethod]
        public void TestExponentielleReelrPositif()
        {
            double resultat = Scientifique.ExponentielleBAseE(1.01);
            Assert.AreEqual(resultat, 2.7456010150169164939897763166604);
        }
        [TestMethod]
        public void TestExponentielleReelNegatif()
        {
            double resultat = Scientifique.ExponentielleBAseE(-1.01);
            Assert.AreEqual(resultat, 0.36421898,0.000000001);
        }

    }
}
