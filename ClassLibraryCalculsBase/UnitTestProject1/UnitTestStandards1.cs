﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClassLibraryCalculs;

namespace UnitTestStandards
{
    [TestClass]
    public class UnitTestStandards1
    {
        // Test de la methode addition
        [TestMethod]
        public void TestAdditionEntiers()
        {
            decimal resultat = Standards.addition(5, 3); // on appelle la méthode addition pour calculer 5+3
            Assert.AreEqual(resultat, 8); // on vérifie que le resultat obtenu vaut 8
        }

       [TestMethod]
        public void TestAdditionfloat()
        {
            decimal resultat = Standards.addition(5.2m, 3.14m); // on appelle la méthode addition pour calculer 5.2 + 3.14
            Assert.AreEqual(resultat, 8.34m); // on vérifie que le resultat obtenu vaut 8.34
        }


        //test de la méthode soustraction
        [TestMethod]
        public void TestSoustractionEntiers()
        {
            decimal resultat = Standards.soustraction(5, 3); // on appelle la méthode addition pour calculer 5.2 + 3.14
            Assert.AreEqual(resultat, 2); // on vérifie que le resultat obtenu vaut 8.34
        }

        [TestMethod]
        public void TestSoustractionfloat()
        {
            decimal resultat = Standards.soustraction(5.2m, 3.14m); // on appelle la méthode addition pour calculer 5.2 + 3.14
            Assert.AreEqual(resultat, 2.06m); // on vérifie que le resultat obtenu vaut 8.34
        }

        // test de la méthode multiplication

        [TestMethod]
        public void TestMultiplicationEntiers()
        {

            decimal resultat = Standards.multiplication(5m, -9m);
            Assert.AreEqual(resultat, -45);
        }

        [TestMethod]
        public void TestMultiplicationReels()
        {

            decimal resultat = Standards.multiplication(5, -9.31m);
            Assert.AreEqual(resultat, -46.55m);
        }
        // test pour la division
        [TestMethod]
        public void TestDivisionEntiers()
        {

            decimal resultat = Standards.division(45, -9);
            Assert.AreEqual(resultat, -5);
        }
        [TestMethod]
        public void TestDivisionReels()
        {

            decimal resultat = Standards.division(5.1m,1.2m);
            Assert.AreEqual(resultat, 4.25m);
        }

        [TestMethod]
        [ExpectedException (typeof (DivideByZeroException))]

        public void TestDivisionParZero()
        {

            Standards.division(100, 0.0m);
        }



    }
}
