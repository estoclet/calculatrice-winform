﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibraryCalculs
{
    public class Scientifique
    {
        public Scientifique()
        {
        }

        public static float Inverse(float x)
        {
             if (x == 0)
                throw new DivideByZeroException();

            return (1 / x);
        }

        public static double RacineCarree(double x)
        {
           if (x < 0)
                throw new ArgumentOutOfRangeException("argument négatif incompatible, l'argument doit être positif ou nul");
            return (Math.Sqrt(x));
        }

        public static float Carre(float x)
        {
            return (x * x);
        }


        public static double PuissanceN (double x, double exp)
        {
            return Math.Pow(x, exp);
        }

        public static double LogBase10(double x)
        {
            
            if (x <= 0)
                throw new ArithmeticException("argument  incompatible, l'argument doit être positif ");
            
            return Math.Log10(x);
        }

        public static double LogBaseE(double x)
        {
            
            if (x <= 0)
                throw new ArithmeticException("argument  incompatible, l'argument doit être positif ");
            return Math.Log(x);
        }

         public static double ExponentielleBAseE(double x)
        {
            return (Math.Exp(x));
        }



    }
}
