﻿using System;

namespace ClassLibraryCalculs
{
    public class Standards
    {
        public Standards()
        {
        }

        public static decimal addition(decimal nb1, decimal nb2)
        {
            return (nb1 + nb2);
        }

        public static decimal soustraction(decimal nb1, decimal nb2)
        {
            return (nb1 - nb2);
        }

        public static decimal multiplication(decimal nb1, decimal nb2)
        {
            return  (nb1 * nb2);
        }

        public static decimal division(decimal nb1, decimal nb2)
        {
            if (nb2 == 0)
                throw new DivideByZeroException();
            return (nb1 / nb2);
        }
    }
}
