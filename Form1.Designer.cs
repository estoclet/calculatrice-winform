﻿namespace Calculatrice
{
    partial class FormCalc
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCalc));
            this.panelStand = new System.Windows.Forms.Panel();
            this.buttonDiv = new System.Windows.Forms.Button();
            this.buttonEgal = new System.Windows.Forms.Button();
            this.buttonPoint = new System.Windows.Forms.Button();
            this.button0 = new System.Windows.Forms.Button();
            this.buttonMult = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonMoins = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.buttonPlus = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.buttonPlusMoins = new System.Windows.Forms.Button();
            this.buttonC = new System.Windows.Forms.Button();
            this.buttonCE = new System.Windows.Forms.Button();
            this.buttonBackSpace = new System.Windows.Forms.Button();
            this.textBoxMonoLigne = new System.Windows.Forms.TextBox();
            this.panelScient = new System.Windows.Forms.Panel();
            this.buttonExpon = new System.Windows.Forms.Button();
            this.buttonLogNeperien = new System.Windows.Forms.Button();
            this.buttonLogDec = new System.Windows.Forms.Button();
            this.buttonPuissance = new System.Windows.Forms.Button();
            this.buttonRacineCarre = new System.Windows.Forms.Button();
            this.buttonCarre = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.modeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.simpleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scientifiqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.richTextBoxOperation = new System.Windows.Forms.RichTextBox();
            this.panelStand.SuspendLayout();
            this.panelScient.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelStand
            // 
            this.panelStand.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelStand.Controls.Add(this.buttonDiv);
            this.panelStand.Controls.Add(this.buttonEgal);
            this.panelStand.Controls.Add(this.buttonPoint);
            this.panelStand.Controls.Add(this.button0);
            this.panelStand.Controls.Add(this.buttonMult);
            this.panelStand.Controls.Add(this.button3);
            this.panelStand.Controls.Add(this.button2);
            this.panelStand.Controls.Add(this.button1);
            this.panelStand.Controls.Add(this.buttonMoins);
            this.panelStand.Controls.Add(this.button6);
            this.panelStand.Controls.Add(this.button5);
            this.panelStand.Controls.Add(this.button4);
            this.panelStand.Controls.Add(this.buttonPlus);
            this.panelStand.Controls.Add(this.button9);
            this.panelStand.Controls.Add(this.button8);
            this.panelStand.Controls.Add(this.button7);
            this.panelStand.Controls.Add(this.buttonPlusMoins);
            this.panelStand.Controls.Add(this.buttonC);
            this.panelStand.Controls.Add(this.buttonCE);
            this.panelStand.Controls.Add(this.buttonBackSpace);
            this.panelStand.Location = new System.Drawing.Point(272, 121);
            this.panelStand.Name = "panelStand";
            this.panelStand.Size = new System.Drawing.Size(351, 439);
            this.panelStand.TabIndex = 2;
            // 
            // buttonDiv
            // 
            this.buttonDiv.BackColor = System.Drawing.Color.GhostWhite;
            this.buttonDiv.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDiv.Location = new System.Drawing.Point(264, 351);
            this.buttonDiv.Name = "buttonDiv";
            this.buttonDiv.Size = new System.Drawing.Size(80, 80);
            this.buttonDiv.TabIndex = 19;
            this.buttonDiv.Text = "/";
            this.buttonDiv.UseVisualStyleBackColor = false;
            this.buttonDiv.Click += new System.EventHandler(this.operator_click);
            // 
            // buttonEgal
            // 
            this.buttonEgal.BackColor = System.Drawing.Color.YellowGreen;
            this.buttonEgal.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEgal.Location = new System.Drawing.Point(178, 351);
            this.buttonEgal.Name = "buttonEgal";
            this.buttonEgal.Size = new System.Drawing.Size(80, 80);
            this.buttonEgal.TabIndex = 18;
            this.buttonEgal.Text = "=";
            this.buttonEgal.UseVisualStyleBackColor = false;
            this.buttonEgal.Click += new System.EventHandler(this.buttonEgal_Click);
            // 
            // buttonPoint
            // 
            this.buttonPoint.BackColor = System.Drawing.Color.GhostWhite;
            this.buttonPoint.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPoint.Location = new System.Drawing.Point(6, 351);
            this.buttonPoint.Name = "buttonPoint";
            this.buttonPoint.Size = new System.Drawing.Size(80, 80);
            this.buttonPoint.TabIndex = 17;
            this.buttonPoint.Text = ",";
            this.buttonPoint.UseVisualStyleBackColor = false;
            this.buttonPoint.Click += new System.EventHandler(this.clic_bouton);
            // 
            // button0
            // 
            this.button0.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button0.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button0.Location = new System.Drawing.Point(92, 351);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(80, 80);
            this.button0.TabIndex = 16;
            this.button0.Text = "0";
            this.button0.UseVisualStyleBackColor = false;
            this.button0.Click += new System.EventHandler(this.clic_bouton);
            // 
            // buttonMult
            // 
            this.buttonMult.BackColor = System.Drawing.Color.GhostWhite;
            this.buttonMult.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMult.Location = new System.Drawing.Point(264, 265);
            this.buttonMult.Name = "buttonMult";
            this.buttonMult.Size = new System.Drawing.Size(80, 80);
            this.buttonMult.TabIndex = 15;
            this.buttonMult.Text = "x";
            this.buttonMult.UseVisualStyleBackColor = false;
            this.buttonMult.Click += new System.EventHandler(this.operator_click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(178, 265);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(80, 80);
            this.button3.TabIndex = 14;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.clic_bouton);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(92, 265);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(80, 80);
            this.button2.TabIndex = 13;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.clic_bouton);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(6, 265);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 80);
            this.button1.TabIndex = 12;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.clic_bouton);
            // 
            // buttonMoins
            // 
            this.buttonMoins.BackColor = System.Drawing.Color.GhostWhite;
            this.buttonMoins.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMoins.Location = new System.Drawing.Point(264, 179);
            this.buttonMoins.Name = "buttonMoins";
            this.buttonMoins.Size = new System.Drawing.Size(80, 80);
            this.buttonMoins.TabIndex = 11;
            this.buttonMoins.Text = "-";
            this.buttonMoins.UseVisualStyleBackColor = false;
            this.buttonMoins.Click += new System.EventHandler(this.operator_click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(178, 179);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(80, 80);
            this.button6.TabIndex = 10;
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.clic_bouton);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(92, 179);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(80, 80);
            this.button5.TabIndex = 9;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.clic_bouton);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(6, 179);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(80, 80);
            this.button4.TabIndex = 8;
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.clic_bouton);
            // 
            // buttonPlus
            // 
            this.buttonPlus.BackColor = System.Drawing.Color.GhostWhite;
            this.buttonPlus.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPlus.Location = new System.Drawing.Point(264, 93);
            this.buttonPlus.Name = "buttonPlus";
            this.buttonPlus.Size = new System.Drawing.Size(80, 80);
            this.buttonPlus.TabIndex = 7;
            this.buttonPlus.Text = "+";
            this.buttonPlus.UseVisualStyleBackColor = false;
            this.buttonPlus.Click += new System.EventHandler(this.operator_click);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(178, 93);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(80, 80);
            this.button9.TabIndex = 6;
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.clic_bouton);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(92, 93);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(80, 80);
            this.button8.TabIndex = 5;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.clic_bouton);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(6, 93);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(80, 80);
            this.button7.TabIndex = 4;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.clic_bouton);
            // 
            // buttonPlusMoins
            // 
            this.buttonPlusMoins.BackColor = System.Drawing.Color.GhostWhite;
            this.buttonPlusMoins.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPlusMoins.Location = new System.Drawing.Point(264, 7);
            this.buttonPlusMoins.Name = "buttonPlusMoins";
            this.buttonPlusMoins.Size = new System.Drawing.Size(80, 80);
            this.buttonPlusMoins.TabIndex = 3;
            this.buttonPlusMoins.Text = "±";
            this.buttonPlusMoins.UseVisualStyleBackColor = false;
            this.buttonPlusMoins.Click += new System.EventHandler(this.buttonPlusMoins_Click);
            // 
            // buttonC
            // 
            this.buttonC.BackColor = System.Drawing.Color.LightCoral;
            this.buttonC.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonC.Location = new System.Drawing.Point(178, 7);
            this.buttonC.Name = "buttonC";
            this.buttonC.Size = new System.Drawing.Size(80, 80);
            this.buttonC.TabIndex = 2;
            this.buttonC.Text = "C";
            this.buttonC.UseVisualStyleBackColor = false;
            this.buttonC.Click += new System.EventHandler(this.buttonC_Click);
            // 
            // buttonCE
            // 
            this.buttonCE.BackColor = System.Drawing.Color.DarkOrange;
            this.buttonCE.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCE.Location = new System.Drawing.Point(92, 7);
            this.buttonCE.Name = "buttonCE";
            this.buttonCE.Size = new System.Drawing.Size(80, 80);
            this.buttonCE.TabIndex = 1;
            this.buttonCE.Text = "CE";
            this.buttonCE.UseVisualStyleBackColor = false;
            this.buttonCE.Click += new System.EventHandler(this.buttonCE_Click);
            // 
            // buttonBackSpace
            // 
            this.buttonBackSpace.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonBackSpace.Font = new System.Drawing.Font("Digital-7", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBackSpace.Location = new System.Drawing.Point(6, 7);
            this.buttonBackSpace.Name = "buttonBackSpace";
            this.buttonBackSpace.Size = new System.Drawing.Size(80, 80);
            this.buttonBackSpace.TabIndex = 0;
            this.buttonBackSpace.Text = "⌫";
            this.buttonBackSpace.UseVisualStyleBackColor = false;
            this.buttonBackSpace.Click += new System.EventHandler(this.buttonBackSpace_Click);
            // 
            // textBoxMonoLigne
            // 
            this.textBoxMonoLigne.Enabled = false;
            this.textBoxMonoLigne.Font = new System.Drawing.Font("Digital-7", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMonoLigne.Location = new System.Drawing.Point(13, 36);
            this.textBoxMonoLigne.Name = "textBoxMonoLigne";
            this.textBoxMonoLigne.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxMonoLigne.Size = new System.Drawing.Size(721, 71);
            this.textBoxMonoLigne.TabIndex = 3;
            this.textBoxMonoLigne.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panelScient
            // 
            this.panelScient.Controls.Add(this.buttonExpon);
            this.panelScient.Controls.Add(this.buttonLogNeperien);
            this.panelScient.Controls.Add(this.buttonLogDec);
            this.panelScient.Controls.Add(this.buttonPuissance);
            this.panelScient.Controls.Add(this.buttonRacineCarre);
            this.panelScient.Controls.Add(this.buttonCarre);
            this.panelScient.Controls.Add(this.button10);
            this.panelScient.Location = new System.Drawing.Point(629, 121);
            this.panelScient.Name = "panelScient";
            this.panelScient.Size = new System.Drawing.Size(108, 439);
            this.panelScient.TabIndex = 4;
            // 
            // buttonExpon
            // 
            this.buttonExpon.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonExpon.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExpon.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonExpon.Location = new System.Drawing.Point(3, 375);
            this.buttonExpon.Name = "buttonExpon";
            this.buttonExpon.Size = new System.Drawing.Size(102, 60);
            this.buttonExpon.TabIndex = 6;
            this.buttonExpon.Text = "ⅇ";
            this.buttonExpon.UseVisualStyleBackColor = false;
            this.buttonExpon.Click += new System.EventHandler(this.clic_bouton);
            // 
            // buttonLogNeperien
            // 
            this.buttonLogNeperien.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonLogNeperien.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLogNeperien.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonLogNeperien.Location = new System.Drawing.Point(3, 313);
            this.buttonLogNeperien.Name = "buttonLogNeperien";
            this.buttonLogNeperien.Size = new System.Drawing.Size(102, 60);
            this.buttonLogNeperien.TabIndex = 5;
            this.buttonLogNeperien.Text = "㏑";
            this.buttonLogNeperien.UseVisualStyleBackColor = false;
            this.buttonLogNeperien.Click += new System.EventHandler(this.clic_bouton);
            // 
            // buttonLogDec
            // 
            this.buttonLogDec.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonLogDec.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLogDec.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonLogDec.Location = new System.Drawing.Point(3, 251);
            this.buttonLogDec.Name = "buttonLogDec";
            this.buttonLogDec.Size = new System.Drawing.Size(102, 60);
            this.buttonLogDec.TabIndex = 4;
            this.buttonLogDec.Text = "㏒";
            this.buttonLogDec.UseVisualStyleBackColor = false;
            this.buttonLogDec.Click += new System.EventHandler(this.clic_bouton);
            // 
            // buttonPuissance
            // 
            this.buttonPuissance.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonPuissance.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPuissance.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonPuissance.Location = new System.Drawing.Point(3, 189);
            this.buttonPuissance.Name = "buttonPuissance";
            this.buttonPuissance.Size = new System.Drawing.Size(102, 60);
            this.buttonPuissance.TabIndex = 3;
            this.buttonPuissance.Text = "xⁱ";
            this.buttonPuissance.UseVisualStyleBackColor = false;
            this.buttonPuissance.Click += new System.EventHandler(this.clic_bouton);
            // 
            // buttonRacineCarre
            // 
            this.buttonRacineCarre.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonRacineCarre.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRacineCarre.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonRacineCarre.Location = new System.Drawing.Point(3, 127);
            this.buttonRacineCarre.Name = "buttonRacineCarre";
            this.buttonRacineCarre.Size = new System.Drawing.Size(102, 60);
            this.buttonRacineCarre.TabIndex = 2;
            this.buttonRacineCarre.Text = "√";
            this.buttonRacineCarre.UseVisualStyleBackColor = false;
            this.buttonRacineCarre.Click += new System.EventHandler(this.clic_bouton);
            // 
            // buttonCarre
            // 
            this.buttonCarre.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonCarre.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCarre.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonCarre.Location = new System.Drawing.Point(3, 65);
            this.buttonCarre.Name = "buttonCarre";
            this.buttonCarre.Size = new System.Drawing.Size(102, 60);
            this.buttonCarre.TabIndex = 1;
            this.buttonCarre.Text = " x²";
            this.buttonCarre.UseVisualStyleBackColor = false;
            this.buttonCarre.Click += new System.EventHandler(this.clic_bouton);
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button10.Location = new System.Drawing.Point(3, 3);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(102, 60);
            this.button10.TabIndex = 0;
            this.button10.Text = "1/x";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.clic_bouton);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(747, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // modeToolStripMenuItem
            // 
            this.modeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.simpleToolStripMenuItem,
            this.scientifiqueToolStripMenuItem,
            this.quitterToolStripMenuItem});
            this.modeToolStripMenuItem.Name = "modeToolStripMenuItem";
            this.modeToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.modeToolStripMenuItem.Text = "Menu";
            // 
            // simpleToolStripMenuItem
            // 
            this.simpleToolStripMenuItem.Name = "simpleToolStripMenuItem";
            this.simpleToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.simpleToolStripMenuItem.Text = "Mode simple";
            // 
            // scientifiqueToolStripMenuItem
            // 
            this.scientifiqueToolStripMenuItem.Name = "scientifiqueToolStripMenuItem";
            this.scientifiqueToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.scientifiqueToolStripMenuItem.Text = "Mode scientifique";
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.quitterToolStripMenuItem.Text = "Quitter";
            this.quitterToolStripMenuItem.Click += new System.EventHandler(this.quitterToolStripMenuItem_Click);
            // 
            // richTextBoxOperation
            // 
            this.richTextBoxOperation.Enabled = false;
            this.richTextBoxOperation.Location = new System.Drawing.Point(12, 124);
            this.richTextBoxOperation.Name = "richTextBoxOperation";
            this.richTextBoxOperation.Size = new System.Drawing.Size(254, 436);
            this.richTextBoxOperation.TabIndex = 5;
            this.richTextBoxOperation.Text = "";
            // 
            // FormCalc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(747, 601);
            this.Controls.Add(this.richTextBoxOperation);
            this.Controls.Add(this.textBoxMonoLigne);
            this.Controls.Add(this.panelStand);
            this.Controls.Add(this.panelScient);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormCalc";
            this.Text = "Calculatrice";
            this.panelStand.ResumeLayout(false);
            this.panelScient.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelStand;
        private System.Windows.Forms.TextBox textBoxMonoLigne;
        private System.Windows.Forms.Button buttonDiv;
        private System.Windows.Forms.Button buttonEgal;
        private System.Windows.Forms.Button buttonPoint;
        private System.Windows.Forms.Button button0;
        private System.Windows.Forms.Button buttonMult;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonMoins;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button buttonPlus;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button buttonPlusMoins;
        private System.Windows.Forms.Button buttonC;
        private System.Windows.Forms.Button buttonCE;
        private System.Windows.Forms.Button buttonBackSpace;
        private System.Windows.Forms.Panel panelScient;
        private System.Windows.Forms.Button buttonExpon;
        private System.Windows.Forms.Button buttonLogNeperien;
        private System.Windows.Forms.Button buttonLogDec;
        private System.Windows.Forms.Button buttonPuissance;
        private System.Windows.Forms.Button buttonRacineCarre;
        private System.Windows.Forms.Button buttonCarre;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem modeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem simpleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scientifiqueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem;
        private System.Windows.Forms.RichTextBox richTextBoxOperation;
    }
}

