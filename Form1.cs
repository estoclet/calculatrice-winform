﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibraryCalculs;

namespace Calculatrice
{
    public partial class FormCalc : Form
    {
        Decimal valeurResultat = 0;
        String operationEffectuee = "";
        bool operationEstEffectuee = false;
        bool operatorClique = false; // Destiné à ne pas prendre en compte des appuis successifs sur des operateurs
        public FormCalc()
        {
            InitializeComponent();
        }

        private void clic_bouton(object sender, EventArgs e)
        {
            operatorClique = false;
            if ((textBoxMonoLigne.Text == "0") || (operationEstEffectuee == true))
                textBoxMonoLigne.Clear();

            operationEstEffectuee = false;
            Button button = (Button)sender;
            if (button.Text == ".")
            {
                if (!textBoxMonoLigne.Text.Contains("."))
                    textBoxMonoLigne.Text = textBoxMonoLigne.Text + button.Text;

            }
            else
                textBoxMonoLigne.Text = textBoxMonoLigne.Text + button.Text;


        }

        private void buttonBackSpace_Click(object sender, EventArgs e)
        {
            textBoxMonoLigne.Text = textBoxMonoLigne.Text.Substring(0, textBoxMonoLigne.Text.Length - 1);
        }

        private void buttonPlusMoins_Click(object sender, EventArgs e)
        {

        }

        private void operator_click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            Standards operationStandard = new Standards();
            operatorClique = true;

            actualisationAffichageHisto(textBoxMonoLigne.Text, operationEffectuee);

            if (valeurResultat != 0 && operationEstEffectuee == false)
            {
                buttonEgal.PerformClick();
                operationEffectuee = button.Text;
                operationEstEffectuee = true;
            }
            else
            {
                operationEffectuee = button.Text;
                valeurResultat = Decimal.Parse(textBoxMonoLigne.Text);
                operationEstEffectuee = true;
            }
        }

        private void buttonCE_Click(object sender, EventArgs e)
        {
            textBoxMonoLigne.Text = "0";
        }

        private void buttonC_Click(object sender, EventArgs e)
        {
            textBoxMonoLigne.Text = "0";
            valeurResultat = 0;
        }

        private void buttonEgal_Click(object sender, EventArgs e)
        {

            if (operatorClique == false)
            {
                actualisationAffichageHisto(textBoxMonoLigne.Text, "=");
                switch (operationEffectuee)
                {
                    case "+":
                        textBoxMonoLigne.Text = (Standards.addition(valeurResultat, Decimal.Parse(textBoxMonoLigne.Text)).ToString());
                        
                        break;
                    case "-":
                        textBoxMonoLigne.Text = (Standards.soustraction(valeurResultat, Decimal.Parse(textBoxMonoLigne.Text)).ToString());
                        break;
                    case "x":
                        textBoxMonoLigne.Text = (Standards.multiplication(valeurResultat, Decimal.Parse(textBoxMonoLigne.Text)).ToString());
                        break;
                    case "/":
                        textBoxMonoLigne.Text = (Standards.division(valeurResultat, Decimal.Parse(textBoxMonoLigne.Text)).ToString());
                        break;
                    default:
                        break;
                }
            }
            valeurResultat = Decimal.Parse(textBoxMonoLigne.Text);
        }

        private void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
        
        private void actualisationAffichageHisto(string operande, string operateur)
        {
            richTextBoxOperation.AppendText(operande + " " + operateur + " ");
        }
    }
}
